export interface Casa {
    $key?: string; //Angular necesita este campo.
    email: string;
    desde: string;
    hasta: string;
    adornos: number;
    lat: number;
    lng: number;
  }