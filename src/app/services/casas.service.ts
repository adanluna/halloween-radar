import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CasasService {
  constructor(
    private firestore: AngularFirestore
  ) {}
  
  //Crea un nuevo casa
  public createCasa(data: any) {
    return this.firestore.collection('casas').add(data);
  }
  //Obtiene un casa
  public getCasa(documentId: string) {
    return this.firestore.collection('casas').doc(documentId).snapshotChanges();
  }
  //Obtiene todos los casas
  public getCasas() {
    return this.firestore.collection('casas').snapshotChanges();
  }
  //Actualiza un casa
  public updateCasa(documentId: string, data: any) {
    return this.firestore.collection('casas').doc(documentId).set(data);
  }

   //Borra un casa
   public deleteCasa(documentId: string) {
    return this.firestore.collection('casas').doc(documentId).delete();
  }
}