import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CasasService } from '../services/casas.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {

  @Input() lat: string;
  @Input() lng: string;

  public casaForm: FormGroup;
  public documentId = null;
  public currentStatus = 1;
  
  constructor(public modal: NgbActiveModal, public fb: FormBuilder, private casasService: CasasService) {      
  }

  onSubmit() {
    if (this.casaForm.valid) {
      console.log("Form Submitted!");
      this.storeCasa(null);
      this.casaForm.reset();
      this.modal.close('guardar');
    }else{
      console.log(this.casaForm.value);
    }
  }

  ngOnInit() {
    this.casaForm = this.fb.group({
      direccion: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.pattern("[^ @]*@[^ @]*")])],
      desde: ['18:00', Validators.required],
      hasta: ['18:00', Validators.required],
      lat: [this.lat],
      lng: [this.lng],
      adornos: ['']
    });
  }

  ngAfterViewInit(){}

  public storeCasa(documentId:any) {
    let form = this.casaForm.value;
    if (documentId == null) {
      let data = {
        email: form.email,
        direccion: form.direccion,
        desde: form.desde,
        hasta: form.hasta,
        adornos: form.adornos,
        lat: form.lat,
        lng: form.lng,
      }
      this.casasService.createCasa(data).then(() => {
        console.log('Documento creado exitósamente!');
        this.casaForm.setValue({
          email: '',
          direccion: '',
          desde: '18:00',
          hasta: '18:00',
          adornos: '',
          lat: '',
          lng: '',
        });
      }, (error) => {
        console.error(error);
      });
    } else {
      let data = {
        email: form.email,
        direccion: form.direccion,
        desde: form.desde,
        hasta: form.hasta,
        adornos: form.adornos,
        lat: form.lat,
        lng: form.lng,
      }
      this.casasService.updateCasa(documentId, data).then(() => {
        this.currentStatus = 1;
        this.casaForm.setValue({
          email: '',
          direccion: '',
          desde: '18:00',
          hasta: '18:00',
          adornos: '',
          lat: '',
          lng: '',
        });
        console.log('Documento editado exitósamente');
      }, (error) => {
        console.log(error);
      });
    }
  } 

}