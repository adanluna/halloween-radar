import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { MapInfoWindow, MapMarker} from '@angular/google-maps';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormularioComponent } from './formulario/formulario.component';
import { CasasService } from './services/casas.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit{
  apiLoaded: Observable<boolean>;
  @ViewChild(MapInfoWindow) infoWindow: MapInfoWindow;

  public casas: any;
  public current: {
    documentId: "",
    lat: "",
    lng: "",
    email: "",
    direccion: "",
    desde: "",
    hasta: "",
    adornos: "",    
  }
  public options: google.maps.MapOptions;
  public markerPositions: google.maps.LatLngLiteral[] = [];
  public homePosition: google.maps.LatLngLiteral;
  public markerClustererImagePath ='https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m';
  public markerIcon = {
    icon: 'https://oundmedia.com/halloween.png',
  };
  public homeIcon = {
    icon: "https://oundmedia.com/pngegg.png",
  };

  constructor(
    httpClient: HttpClient, 
    private modalService: NgbModal,
    private casasService: CasasService,
    ) {
    this.apiLoaded = httpClient.jsonp('https://maps.googleapis.com/maps/api/js?key=AIzaSyAd4W038WbLTsQA9MguKs_vwLrTlnYJBxU', 'callback')
        .pipe(
          map(() => true),
          catchError(() => of(false)),
        );

    this.options = {
      center: {lat: 25.7664082, lng: -100.4276889},
      zoom: 15
    };
  }

  ngOnInit() {
    this.casasService.getCasas().subscribe((casasSnapshot) => {
      this.casas = [];
      casasSnapshot.forEach((casaData: any) => {
        let data = casaData.payload.doc.data();
        this.casas.push({
          documentId: casaData.payload.doc.id,
          lat: data.lat,
          lng: data.lng,
          email: data.email,
          direccion: data.direccion,
          desde: data.desde,
          hasta: data.hasta,
          adornos: data.adornos,
        });
      });
      this.markerPositions = this.casas;
    });
    this.getLocation();
  }

  addMarker(event: google.maps.MapMouseEvent) {
    let marker = event.latLng.toJSON();
    //this.markerPositions.push(marker);
    this.openVerticallyCentered(marker);
  }

  openInfoWindow(marker: MapMarker, markerPosition: any) {
    this.current = markerPosition;
    this.infoWindow.open(marker);
  }

  openVerticallyCentered(marker:any) {
    let modalRef = this.modalService.open(FormularioComponent, {centered: true, ariaLabelledBy: 'modal-basic-title'});
    modalRef.componentInstance.lat = marker.lat;
    modalRef.componentInstance.lng = marker.lng;
    modalRef.result.then((result) => {
    }, (reason) => {
      //this.markerPositions.pop();
    });
  } 

  public delete(documentId:any) {
    if (documentId != null) {
      this.casasService.deleteCasa(documentId).then(() => {
        console.log('Documento borrado!');
      }, (error) => {
        console.error(error);
      });
    }
  } 

getLocation() {
  if (navigator.geolocation) {
      var self = this;
      navigator.geolocation.getCurrentPosition(function(response){
          self.showPosition(response, self);
      }, function() {
      alert("Unable to get GPS Location");
      }, {
      enableHighAccuracy : true
      });
  }
  else {
    alert("Geolocation is not supported by this browser.");
  }
}

showPosition(position:any, self:any) {
  this.homePosition = {lat: position.coords.latitude, lng: position.coords.longitude};
  }
}