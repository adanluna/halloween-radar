// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    projectId: 'halloween-radar',
    appId: '1:233152077396:web:f3417127f669c3bc6c17c1',
    storageBucket: 'halloween-radar.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyDXwvNPz6JUhAAWf16dSvR7Zs4rEMRpVxo',
    authDomain: 'halloween-radar.firebaseapp.com',
    messagingSenderId: '233152077396',
    measurementId: 'G-YSFQVJD47V',
  }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
